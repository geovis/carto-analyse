var map = L.map('mapdiv', {
  minZoom: 7,
  maxBounds: [[45, 3], [49, 13]]
}).setView([46.9, 8.2], 8);

var styleFn = function(feature){
  var v = feature.properties.rapport_gde_petites_entreprises;
  return {
    fillColor: brew.getColorInRange(v),
    fillOpacity: 1,
    opacity: 1, 
    weight: 0.4, 
    color: '#ffffff', 
  };
};


var brew;
var layers = {};


fetch('/cantons')
  .then(function(response){ return response.json(); })
  .then(function(data){
    // Extraction des valeurs en vue de la mise en classes
    var valeurs = [];
    for (var i in data.features){
      var feat = data.features[i];
      valeurs.push(feat.properties.rapport_gde_petites_entreprises);
    }

    // Nous pouvons maintenant faire la mise en classes
    brew = new classyBrew();
    brew.setSeries(valeurs);
    brew.setNumClasses(5);
    brew.setColorCode("Reds");
    brew.classify('jenks');
  
    layers.cantons = L.geoJSON(
      data, {
        style: styleFn,
        attribution: 'Fond de carte: OFS, ThemaKart, 2012 | Données: OFS, 2016'
      }
    ).addTo(map);

    add_legend(map, brew);

    sort_layers();
  });


fetch('/suisse')
  .then(function(response){ return response.json(); })
  .then(function(data){
    layers.suisse_fill = L.geoJSON(data, {
      style: {
        fillColor: '#bbb', fillOpacity: 1,
        opacity: 0, weight: 0
      }
    }).addTo(map);

    layers.suisse_line = L.geoJSON(data, {
      style: {
        fillOpacity: 0, opacity: 1,
        weight: 0.8, color: '#000'
      }
    }).addTo(map);

    sort_layers();
  });


fetch('/lacs')
  .then(function(response){ return response.json(); })
  .then(function(data){
    layers.lacs = L.geoJSON(data, {
      style: {
        fillColor: '#999', fillOpacity: 1,
        opacity: 0
      }
    }).addTo(map);

    sort_layers();
  });


// La légende
var add_legend = function(map, brew){
  var breaks = brew.getBreaks(),
      colors = brew.getColors();

  var n = colors.length;

  var legend = L.control({position: 'topright'});

  legend.onAdd = function(map){
    var div = L.DomUtil.create('div', 'legend');
    var svg = '<svg width="130" height="' + (colors.length*20+35) + '">';
    for (var i=0; i < n; i++){
      svg += '<rect x="1" y="'+ (10+i*20) +'" width="35" height="19" fill="'+colors[n-i-1]+'" stroke="#fff" stroke-width="1" />';
    }
    svg += '<rect x="1" y="10" width="35" height="'+(n*20-1)+'" fill="none" stroke="#000" stroke-width="0.4" />';
    for (var i=0; i<n; i++){
      svg += '<line x1="36" y1="'+(10+i*20)+'" x2="42" y2="'+(10+i*20)+'" stroke="#000" stroke-width="0.4" />';
      svg += '<text x="45" y="'+ (14+i*20) +'">'+ Math.round(breaks[n-i]*1000)/10 +'</text>';
    }
    svg += '<line x1="36" y1="'+(9+n*20)+'" x2="42" y2="'+(9+n*20)+'" stroke="#000" stroke-width="0.4" />';
    svg += '<text x="45" y="'+ (13+n*20) +'">'+ Math.round(breaks[n-i]*1000)/10 +'</text>';
    svg +=    '</svg>';
    div.innerHTML = '<p>Nombre de grandes entreprises par 100 petites entreprises</p>' + svg;
    return div;
  };

  legend.addTo(map);
}


L.control.scale({metric: true, imperial: false}).addTo(map);


// L'ensemble des couches sont chargées de manière asynchrone, c'est-à-dire
// leur ordre d'arrivée n'est pas définie, et du coup l'ordre dans Leaflet
// ne l'est pas non plus. Du coup, après chaque chargement d'une couche, nous
// devons appeler cette fonction qui définir l'ordre des couches.
var sort_layers = function(){
  // Nous passons à travers toutes les couches qui se trouvent dans `layers`.
  // Si la couche est définie, nous la plaçons au bon niveau.
  if (layers.cantons) layers.cantons.bringToFront();
  if (layers.suisse_fill) layers.suisse_fill.bringToBack()
  if (layers.suisse_line) layers.suisse_line.bringToFront();
  if (layers.lacs) layers.lacs.bringToFront();
}
